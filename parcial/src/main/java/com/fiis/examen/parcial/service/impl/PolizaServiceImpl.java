package com.fiis.examen.parcial.service.impl;


import com.fiis.examen.parcial.dto.AsociarPolizaClienteRequest;
import com.fiis.examen.parcial.service.PolizaService;

public class PolizaServiceImpl implements PolizaService {
    @Override
    public void asociarPolizaConCliente(AsociarPolizaClienteRequest request) {
        System.out.println("Asociando cliente de DNI-->"+ request.getCliente().getDni());
        System.out.println("Tipo de poliza a asociar-->"+ request.getPolizaSeguro().toString());
    }
}
