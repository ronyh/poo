package com.fiis.examen.parcial.dto;

import com.fiis.examen.parcial.domain.Cliente;
import com.fiis.examen.parcial.domain.PolizaSeguro;

public class AsociarPolizaClienteRequest {
    Cliente cliente;
    PolizaSeguro polizaSeguro;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public PolizaSeguro getPolizaSeguro() {
        return polizaSeguro;
    }

    public void setPolizaSeguro(PolizaSeguro polizaSeguro) {
        this.polizaSeguro = polizaSeguro;
    }

    public AsociarPolizaClienteRequest(Cliente cliente, PolizaSeguro polizaSeguro) {
        this.cliente = cliente;
        this.polizaSeguro = polizaSeguro;
    }
}
