package pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl;

import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.Operable;

public class Suma implements Operable {
    public Double operar(Double op1, Double op2) {
        return op1+op2;
    }
}
