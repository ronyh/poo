package pe.uni.fiis.poo.demo.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.uni.fiis.poo.demo.domain.Producto;
import pe.uni.fiis.poo.demo.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductoMapper  implements RowMapper<Producto> {

    public Producto mapRow(ResultSet resultSet, int i)
            throws SQLException {
        Producto response = new Producto();
        response.setCoProducto(resultSet.getString("cod_producto"));
        response.setNombres(resultSet.getString("nombres"));
        response.setEstado(resultSet.getString("estado"));
        return response;
    }
}
