package pe.edu.uni.fiis.prueba.parcial.calculadora.servicio;

public interface Operable {
    public Double operar(Double op1, Double op2);
}
