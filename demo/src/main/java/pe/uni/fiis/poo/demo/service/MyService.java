package pe.uni.fiis.poo.demo.service;

import pe.uni.fiis.poo.demo.domain.Libro;
import pe.uni.fiis.poo.demo.domain.Producto;
import pe.uni.fiis.poo.demo.dto.*;

import java.util.List;

public interface MyService {
    UsuarioResponse loginUsuario(UsuarioRequest request);
    UsuarioResponse actualizarCredencial(UsuarioRequest request);
    UsuarioResponse removerUsuario(UsuarioRequest request);
    UsuarioResponse crearUsuario(UsuarioRequest request);
    List<Producto> obtenerProductos();
    List <Libro> obtenerLibros();
    List<Libro> listarLibrosPorAutor(LibroPorAutorRequest request);
}
