package pe.edu.uni.fiis.prueba;

import java.util.*;

public class Prueba {
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean equals(Object obj) {
        Prueba d = (Prueba)obj;
        return this.nombre.equals(d.nombre);
    }

    public int hashCode() {
        return this.nombre.hashCode();
    }

    public static void main(String[] args) {
        Prueba prueba1 = new Prueba();
        prueba1.setNombre("sustitutorio");

        Prueba prueba2 = new Prueba();
        prueba2.setNombre("sustitutorio1");


        List<Prueba> lista = new ArrayList<Prueba>();
        lista.add(prueba1);
        lista.add(prueba2);

        Collections.sort(lista, new Comparator<Prueba>() {
            public int compare(Prueba o1, Prueba o2) {
                return o1.getNombre().compareTo(o2.getNombre());
            }
        });

        Set<Prueba> set1 = new HashSet<Prueba>();
        set1.add(prueba1);
        set1.add(prueba2);

        Map<String,Prueba> mapa = new HashMap<String,Prueba>();
        mapa.put("rony" ,prueba1);
        mapa.put("jordan" ,prueba2);

        System.out.println("***********");
        System.out.println(mapa.get("rony").getNombre());
        System.out.println("***********");
        System.out.println(mapa.get("jordan").getNombre());

        for (Prueba i: lista) {
            System.out.println(i.getNombre());
        }
        System.out.println("***********");
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).getNombre());
        }
        System.out.println("***********");
        for (int i = 0; i < set1.size(); i++) {
            System.out.println(lista.get(i).getNombre());
        }

    }
}
