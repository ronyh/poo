package com.fiis.examen.parcial.service;

import com.fiis.examen.parcial.dto.AsociarPolizaClienteRequest;

public interface PolizaService {
    void asociarPolizaConCliente(AsociarPolizaClienteRequest request);
}
