package pe.edu.uni.fiis.excel;

public enum Color {
    BLUE("azul","FFFFF0"),
    GREEN("verde","00FF00");
    private String nombre;
    private String hexa;
    Color(String nombre, String hexa){
        this.hexa=hexa;
        this.nombre=nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public String getHexa() {
        return hexa;
    }
}
