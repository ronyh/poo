package pe.uni.fiis.poo.demo.dto;

import pe.uni.fiis.poo.demo.domain.Libro;

import java.util.List;

public class LibroResponse {
    private List<Libro> lista;

    public List<Libro> getLista() {
        return lista;
    }

    public void setLista(List<Libro> lista) {
        this.lista = lista;
    }
}
