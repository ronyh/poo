package pe.uni.fiis.poo.demo.dao.mapper;


import org.springframework.jdbc.core.RowMapper;
import pe.uni.fiis.poo.demo.domain.Usuario;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioMapper  implements RowMapper<Usuario> {
    @Override
    public Usuario mapRow(ResultSet resultSet, int i)
            throws SQLException {
        Usuario response = new Usuario();
        response.setCoUsuario(resultSet.getString("cod_usuario"));
        response.setCredencial(resultSet.getString("credencial"));
        response.setNombres(resultSet.getString("nombres"));
        response.setApellidos(resultSet.getString("apellidos"));
        response.setEstado(resultSet.getString("estado"));
        return response;
    }
}
