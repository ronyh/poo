package pe.uni.fiis.poo.demo.dto;

import pe.uni.fiis.poo.demo.domain.Usuario;

public class UsuarioResponse {
    private String error;
    private Usuario usuario;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
