package pe.edu.uni.fiis.polimorfismo;

public class Epson extends Impresora
        implements Eficiente,Renovable{
    public String imprimir() {
        return "epson";
    }

    public Integer potenciar() {
        return 19;
    }
    public String reciclar() {
        return "renovación";
    }
    public String mensaje(){
        System.out.println("mensaje Epson");
        return "mensaje Epson";
    }
    public String mensaje(String dato){
        System.out.println("mensaje"+dato);
        return "mensaje Epson";
    }
}
