package pe.edu.uni.fiis.polimorfismo;

public class Principal {
    public static void main(String[] args) {
        //Impresora impresora = new Impresora();
        Impresora epson = new Epson();
        System.out.println(epson.imprimir());
        epson.mensaje();
        Impresora hp = new Hp();
        System.out.println(hp.imprimir());

        Eficiente eficiente = new Epson();
        eficiente.potenciar();
        Epson epson1 = new Epson();
        epson1.mensaje(" epson2");
    }
}
