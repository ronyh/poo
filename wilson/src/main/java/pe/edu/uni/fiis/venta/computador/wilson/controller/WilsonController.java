package pe.edu.uni.fiis.venta.computador.wilson.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pe.edu.uni.fiis.venta.computador.wilson.domain.Alumno;
import pe.edu.uni.fiis.venta.computador.wilson.dto.AlumnoPorCodRequest;
import pe.edu.uni.fiis.venta.computador.wilson.dto.AlumnoPorCodResponse;

@RestController
@RequestMapping("/servicio")
public class WilsonController {
    @RequestMapping(value = "/alumnoByCod",method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody AlumnoPorCodResponse buscarAlumnoPorCodigo(
            @RequestBody AlumnoPorCodRequest alumnoPorCodRequest){
        AlumnoPorCodResponse alumnoPorCodResponse = new AlumnoPorCodResponse();
        Alumno alumno = new Alumno();
        alumno.setCoAlumno("020020");
        alumno.setApellidos("Carpio");
        alumno.setNombres("Jordan");
        alumno.setEdad(85);
        alumnoPorCodResponse.setAlumno(alumno);
        return alumnoPorCodResponse;
    }
}
