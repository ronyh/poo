create table eduardo_sector(
    id_sector numeric(9,0),
    sector varchar(50)

);
select * from eduardo_sector;
insert into eduardo_sector (id_sector, sector)
values (1,'Agropecuario');

create table eduardo_comercial(
    id_comercial numeric(9,0),
    comercial varchar(50)
);
select * from eduardo_comercial;
insert into eduardo_comercial(id_comercial, comercial)
values (1,'Gamarra');
insert into eduardo_comercial(id_comercial, comercial)
values (2,'Polvos Azules');
insert into eduardo_comercial(id_comercial, comercial)
values (3,'El Hueco');
create table eduardo_cliente(
    empresa varchar(50),
    persona_contacto varchar(50),
    direccion varchar(50),
    pais numeric(9,0),
    sector numeric(9,0),
    comercial numeric(9,0),
    facturado varchar(50)
);
select * from eduardo_cliente;
create table eduardo_pais(
    id_pais numeric(9,0),
    pais varchar(50),
    capital varchar(50),
    continente numeric(9,0)
);
select * from eduardo_pais;
create table eduardo_continente(
    id_continente numeric(9,0),
    delegado varchar(50),
    continente numeric(9,0)
);
alter table eduardo_continente add primary key (delegado);
select * from eduardo_continente;
alter table eduardo_continente alter column continente type varchar(50);
delete from eduardo_continente;
update eduardo_continente
set delegado = 'Carlos'
where id_continente = 2;

insert into eduardo_continente(id_continente, delegado, continente)
values (1,'Mariano','Asia');
insert into eduardo_continente(id_continente, delegado, continente)
values (2,'Sebastian','America');
insert into eduardo_continente(id_continente, delegado, continente)
values (3,'Renzo','Europa');
select * from oc_continente;
insert into oc_continente(id_continente, continente, delegado)
values (3,'America','Eduardo');
commit ;
rollback ;


create table rony(
    codigo varchar(40)  ,
    secuencia varchar(2) ,
    nombre varchar(20),
    primary key (codigo,secuencia)
);

create table poo(
     identificador varchar(35),
     codigo varchar(40)  ,
     secuencia varchar(2) ,
     descripcion varchar(20),
     primary key (identificador),
     foreign key (codigo,secuencia) references
         rony(codigo, secuencia)
);

select * from rony;


alter table eduardo_continente add primary key (delegado);
ALTER TABLE poo ADD FOREIGN KEY (identificador)
    REFERENCES eduardo_continente(delegado);

select p.nomProducto
from ventasmetas v
inner join producto p
on(v.co_producto = p.co_producto)