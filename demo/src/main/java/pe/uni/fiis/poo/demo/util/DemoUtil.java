package pe.uni.fiis.poo.demo.util;

import pe.uni.fiis.poo.demo.domain.Usuario;
import pe.uni.fiis.poo.demo.dto.UsuarioRequest;

public class DemoUtil {

    public static Usuario mapToUsuario(UsuarioRequest request){
        Usuario response = new Usuario();
        response.setCredencial(request.getCredencial());
        response.setCoUsuario(request.getCoUsuario());
        response.setEstado(request.getEstado());
        response.setApellidos(request.getApellidos());
        response.setNombres(request.getNombres());
        return response;
    }

}
