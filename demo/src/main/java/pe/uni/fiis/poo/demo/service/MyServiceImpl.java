package pe.uni.fiis.poo.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.uni.fiis.poo.demo.dao.MyDao;
import pe.uni.fiis.poo.demo.domain.Libro;
import pe.uni.fiis.poo.demo.domain.Producto;
import pe.uni.fiis.poo.demo.domain.Usuario;
import pe.uni.fiis.poo.demo.dto.*;
import pe.uni.fiis.poo.demo.util.DemoUtil;

import java.util.List;

@Service
public class MyServiceImpl implements MyService{


    @Autowired
    private MyDao myDao;

    public List<Libro> listarLibrosPorAutor(LibroPorAutorRequest request) {
        return this.myDao.listarLibrosPorAutor(request);
    }
    public UsuarioResponse loginUsuario(UsuarioRequest request) {

        Usuario usuario = this.myDao.loginUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response = new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }
    public List<Producto> obtenerProductos(){
        return this.myDao.obtenerProductos();
    }
    public UsuarioResponse actualizarCredencial(UsuarioRequest request) {

        Usuario usuario = this.myDao.actualizarCredencial(DemoUtil.mapToUsuario(request));
        UsuarioResponse response = new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }

    public UsuarioResponse crearUsuario(UsuarioRequest request) {
        Usuario usuario = this.myDao.crearUsuarioAutogenerado(DemoUtil.mapToUsuario(request));
        UsuarioResponse response = new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }

    public List<Libro> obtenerLibros() {
        return this.myDao.obtenerLibros();
    }



    public UsuarioResponse removerUsuario(UsuarioRequest request) {

        Usuario usuario = this.myDao.removerUsuario(DemoUtil.mapToUsuario(request));
        UsuarioResponse response = new UsuarioResponse();
        response.setUsuario(usuario);
        return response;
    }
}
