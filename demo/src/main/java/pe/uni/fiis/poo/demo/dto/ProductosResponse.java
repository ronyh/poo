package pe.uni.fiis.poo.demo.dto;

import pe.uni.fiis.poo.demo.domain.Producto;

import java.util.List;

public class ProductosResponse {
    private List<Producto> lista;

    public List<Producto> getLista() {
        return lista;
    }

    public void setLista(List<Producto> lista) {
        this.lista = lista;
    }
}
