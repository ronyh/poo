package pe.uni.fiis.poo.demo.dao;

import pe.uni.fiis.poo.demo.domain.Libro;
import pe.uni.fiis.poo.demo.domain.Producto;
import pe.uni.fiis.poo.demo.domain.Usuario;
import pe.uni.fiis.poo.demo.dto.LibroPorAutorRequest;

import java.util.List;

public interface MyDao {
    Usuario loginUsuario(Usuario request);
    List<Producto> obtenerProductos();
    Usuario actualizarCredencial(Usuario request);
    Usuario removerUsuario(Usuario request);
    Usuario crearUsuario(Usuario request);
    public List<Libro> obtenerLibros();
    Usuario crearUsuarioAutogenerado(Usuario request);
    List<Libro> listarLibrosPorAutor(LibroPorAutorRequest request);
}
