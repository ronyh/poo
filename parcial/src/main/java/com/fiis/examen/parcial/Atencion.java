package com.fiis.examen.parcial;

import com.fiis.examen.parcial.domain.Accidente;
import com.fiis.examen.parcial.domain.Cliente;
import com.fiis.examen.parcial.domain.PolizaSeguro;
import com.fiis.examen.parcial.domain.TIPO_POLIZA;
import com.fiis.examen.parcial.dto.AsociarPolizaClienteRequest;
import com.fiis.examen.parcial.service.CentroAtencioService;
import com.fiis.examen.parcial.service.PolizaService;
import com.fiis.examen.parcial.service.impl.Clinica;
import com.fiis.examen.parcial.service.impl.PolizaServiceImpl;
import com.fiis.examen.parcial.service.impl.Taller;

public class Atencion {
    public static void main(String[] args) {
        System.out.println("-----Simulando asociacion de poliza----");
        Cliente cliente= new Cliente("01");
        PolizaSeguro polizaSeguro= new PolizaSeguro(TIPO_POLIZA.TIPO_A,"1","A",250);
        AsociarPolizaClienteRequest request= new AsociarPolizaClienteRequest(cliente,polizaSeguro);
        PolizaService polizaService= new PolizaServiceImpl();
        polizaService.asociarPolizaConCliente(request);
        System.out.println("-----Simulando taller----");
        Accidente accidente= new Accidente("Quemadura de tercer grado");
        CentroAtencioService service= new Taller();
        service.atender(accidente);
        service.terminarAtencion();
        System.out.println("-----Simulando clinica----");
        service= new Clinica();
        service.atender(accidente);
        service.terminarAtencion();
    }
}
