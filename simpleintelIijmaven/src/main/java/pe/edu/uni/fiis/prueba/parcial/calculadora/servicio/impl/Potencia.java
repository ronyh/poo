package pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl;

import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.Operable;

public class Potencia implements Operable {
    public Double operar(Double op1, Double op2) {
        return Math.pow(op1,op2);
    }
}
