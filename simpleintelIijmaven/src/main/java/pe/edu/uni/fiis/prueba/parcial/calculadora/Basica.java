package pe.edu.uni.fiis.prueba.parcial.calculadora;

import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.Operable;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Division;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Multiplicacion;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Resta;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Suma;

public class Basica {
    private Operacion operacion;

    public Double sumar(){
        return Operaciones.SUMA.getOperable().operar(
                operacion.getOperando1(),operacion.getOperando2());
    }
    public Double restar(){
        return Operaciones.RESTA.getOperable().operar(
                operacion.getOperando1(),operacion.getOperando2());
    }
    public Double multiplicar(){
        return Operaciones.MULTIPLICACION.getOperable().operar(
                operacion.getOperando1(),operacion.getOperando2());
    }
    public Double dividir(){
        return Operaciones.DIVISION.getOperable().operar(
                operacion.getOperando1(),operacion.getOperando2());
    }

    public Operacion getOperacion() {
        return operacion;
    }

    public void setOperacion(Operacion operacion) {
        this.operacion = operacion;
    }
}
