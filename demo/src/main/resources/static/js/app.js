var app = new Vue({
    el: '#content',
    data: {
        pagina:'login',
        usuario:"Rony",
        clave:null,
        nombre: "",
        libros: [
            {
                isbn:"001",
                titulo:"titulo1",
                editorial:"editorial"
            },
            {
                isbn:"002",
                titulo:"titulo2",
                editorial:"editorial2"
            }
        ]
    },
    methods: {
        ingresar : function () {
            //this.usuario = 'Jordan';
            var vm = this;
            var url = 'http://localhost:8080/usuario';
            var data = {
                "coUsuario": this.usuario,
                "credencial": this.clave
            };

            fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => {
                console.log('Success:', response);
                vm.nombre = response.usuario.nombres + " " +response.usuario.apellidos;
                vm.pagina = 'home';
                vm.traerLibros();
            });
        },


        traerLibros : function () {
            var vm = this;
            var url = 'http://localhost:8080/libros';
            var data = {
            };

            fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
                console.log('Success:', response);
            vm.libros = response.lista;
        });
        }

    }
})