package com.fiis.examen.parcial.domain;

public class Cliente {
    private String dni;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Cliente(String dni) {
        this.dni = dni;
    }
}
