package pe.uni.fiis.poo.demo.dto;

public class LibroPorAutorRequest {
    private String autor;

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}
