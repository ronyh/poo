package pe.uni.fiis.poo.demo.domain;

public class Producto {
    /**
     * cod_producto varchar(10),
    nombres varchar(50),
    estado varchar(1)
     */
    private String coProducto;
    private String nombres;
    private String estado;

    public String getCoProducto() {
        return coProducto;
    }

    public void setCoProducto(String coProducto) {
        this.coProducto = coProducto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
