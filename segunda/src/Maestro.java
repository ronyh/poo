public class Maestro extends Docente{
    private Integer experiencia;

    public Maestro() {
        super();
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }
}
