package pe.uni.fiis.poo.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pe.uni.fiis.poo.demo.dto.*;
import pe.uni.fiis.poo.demo.service.MyService;

@RestController
public class MyController {

    @Autowired
    private MyService myService;

    @PostMapping(value = "/usuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse loginUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.loginUsuario(request);
        if(response.getUsuario() == null){
            response.setError("No hay usuario");
        }
        return response;
    }

    @PostMapping(value = "/actualizarUsuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse actualizarCredencial(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.actualizarCredencial(request);
        return response;
    }

    @PostMapping(value = "/eliminarUsuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse removerUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.removerUsuario(request);
        return response;
    }
    @PostMapping(value = "/crearUsuario",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public UsuarioResponse crearUsuario(@RequestBody UsuarioRequest request){
        UsuarioResponse response = this.myService.crearUsuario(request);
        return response;
    }
    @PostMapping(value = "/productos",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductosResponse listarProductos(){
        ProductosResponse response = new ProductosResponse();
        response.setLista(this.myService.obtenerProductos());
        return response;
    }
    @PostMapping(value = "/libros",
             produces = MediaType.APPLICATION_JSON_VALUE)
    public LibroResponse listarLibros(){
        LibroResponse response = new LibroResponse();
        response.setLista(this.myService.obtenerLibros());
        return response;
    }

    @PostMapping(value = "/libros-autor",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody LibroPorAutorResponse listarLibrosPorAutor(@RequestBody LibroPorAutorRequest request){
        LibroPorAutorResponse response = new LibroPorAutorResponse();
        response.setLista(this.myService.listarLibrosPorAutor(request));
        return response;
    }


}
