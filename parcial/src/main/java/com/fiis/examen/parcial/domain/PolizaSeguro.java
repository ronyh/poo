package com.fiis.examen.parcial.domain;

public class PolizaSeguro {
    private TIPO_POLIZA poliza;
    private String numerounico;
    private String tipoSeguro;
    private int sumaAsegurada;

    public PolizaSeguro(TIPO_POLIZA poliza, String numerounico, String tipoSeguro, int sumaAsegurada) {
        this.poliza = poliza;
        this.numerounico = numerounico;
        this.tipoSeguro = tipoSeguro;
        this.sumaAsegurada = sumaAsegurada;
    }

    @Override
    public String toString() {
        return "PolizaSeguro{" +
                "poliza=" + poliza +
                ", numerounico='" + numerounico + '\'' +
                ", tipoSeguro='" + tipoSeguro + '\'' +
                ", sumaAsegurada=" + sumaAsegurada +
                '}';
    }
}
