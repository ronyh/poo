package pe.edu.uni.fiis.prueba.parcial.calculadora;

import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.Operable;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Division;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Multiplicacion;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Resta;
import pe.edu.uni.fiis.prueba.parcial.calculadora.servicio.impl.Suma;

public enum Operaciones {
    SUMA("S"),RESTA("R"),MULTIPLICACION("M"),DIVISION("S");
    private Operaciones(String tipo){
        if (tipo.equals("S")) {
            this.operable = new Suma();
        } else if (tipo.equals("R")) {
            this.operable = new Resta();
        } else if (tipo.equals("M")) {
            this.operable = new Multiplicacion();
        } else if (tipo.equals("D")) {
            this.operable = new Division();
        }
    }
    private Operable operable;

    public Operable getOperable() {
        return operable;
    }
}
