package pe.edu.uni.fiis.venta.computador.wilson.dto;

import pe.edu.uni.fiis.venta.computador.wilson.domain.Alumno;

public class AlumnoPorCodResponse {
    private Alumno alumno;

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
}
