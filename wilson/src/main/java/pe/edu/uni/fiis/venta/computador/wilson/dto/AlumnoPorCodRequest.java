package pe.edu.uni.fiis.venta.computador.wilson.dto;

public class AlumnoPorCodRequest {
    private String coAlumno;

    public String getCoAlumno() {
        return coAlumno;
    }

    public void setCoAlumno(String coAlumno) {
        this.coAlumno = coAlumno;
    }
}
