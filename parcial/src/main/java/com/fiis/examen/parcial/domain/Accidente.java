package com.fiis.examen.parcial.domain;

public class Accidente implements Evento {
    private String tipoAccidente;

    public String getTipoAccidente() {
        return tipoAccidente;
    }

    public void setTipoAccidente(String tipoAccidente) {
        this.tipoAccidente = tipoAccidente;
    }

    public Accidente(String tipoAccidente) {
        this.tipoAccidente = tipoAccidente;
    }

    @Override
    public void darParte() {
        System.out.println("Notificar a mi aseguradora de "+ this.tipoAccidente);
    }
}
