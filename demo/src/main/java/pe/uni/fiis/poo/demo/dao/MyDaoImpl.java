package pe.uni.fiis.poo.demo.dao;

import org.springframework.stereotype.Repository;
import pe.uni.fiis.poo.demo.dao.datasource.MyDatasource;
import pe.uni.fiis.poo.demo.dao.mapper.ProductoMapper;
import pe.uni.fiis.poo.demo.dao.mapper.UsuarioMapper;
import pe.uni.fiis.poo.demo.domain.Libro;
import pe.uni.fiis.poo.demo.domain.Producto;
import pe.uni.fiis.poo.demo.domain.Usuario;
import pe.uni.fiis.poo.demo.dto.LibroPorAutorRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class MyDaoImpl extends MyDatasource implements MyDao {
    public Usuario actualizarCredencial(Usuario request) {
        String sql =    " update Usuario " +
                        " set credencial = ? " +
                        " where cod_usuario = ?";
        this.jdbcTemplate.update(sql,
                new String[]{
                        request.getCredencial(),request.getCoUsuario()
                });
        return request;
    }

    public Usuario crearUsuario(Usuario request) {
        String sql =    " insert into Usuario (cod_usuario,\n" +
                "       credencial,\n" +
                "       nombres,\n" +
                "       apellidos,\n" +
                "       estado)" +
                "       values( ? , ? , ? ,? ,?)";
        this.jdbcTemplate.update(sql,
                new String[]{
                        request.getCoUsuario(),request.getCredencial(),
                        request.getNombres(),request.getApellidos(),
                        "ACTIVE"
                });
        return request;
    }
    public Usuario crearUsuarioAutogenerado(Usuario request) {
        String sql = " select 'UNI'||trim(to_char( " +
                "          to_number(substr(max(cod_usuario),4,7),'9999999')+1" +
                "           ,'0000009')) cod_usuario, " +
                "   null credencial, null nombres, null apellidos, null estado" +
                " from Usuario";
        Usuario usuario = this.jdbcTemplate.queryForObject(sql, new UsuarioMapper());
        request.setCoUsuario(usuario.getCoUsuario());

        return crearUsuario(request);
    }




    public Usuario removerUsuario(Usuario request) {
        String sql =    " delete from Usuario " +
                " where cod_usuario = ?";
        this.jdbcTemplate.update(sql,
                new String[]{
                        request.getCoUsuario()
                });
        return request;
    }

    public List<Libro> listarLibrosPorAutor(LibroPorAutorRequest request) {
        List<Libro> lista = null;
        try{
            List<Map<String, Object>> filas = this.jdbcTemplate.queryForList(
                    " select isbn,\n" +
                            "       titulo,\n" +
                            "       editorial,\n" +
                            "       autor,\n" +
                            "       to_char(fecha,'YYYY-MM-DD HH12:MI:SS') fec from libro " +
                            " where upper(autor) like '%"+request.getAutor().toUpperCase()+"%'");
            lista = new ArrayList<Libro>();
            for (Map<String, Object> fila : filas) {
                Libro p = new Libro();
                p.setTitulo((String)fila.get("titulo"));
                p.setEditorial((String)fila.get("editorial"));
                p.setAutor((String)fila.get("autor"));
                p.setIsbn((String)fila.get("isbn"));
                p.setFecha((String)fila.get("fec"));
                lista.add(p);
            }

        }catch (Exception ex){
            return lista;
        }
        return lista;
    }

    public List<Producto> obtenerProductos() {
        List<Producto> lista = null;
        try{
            List<Map<String, Object>> filas = this.jdbcTemplate.queryForList(
                    " select cod_producto, nombres, estado " +
                            "from producto " +
                            "where estado = '1' ");
            lista = new ArrayList<Producto>();
            for (Map<String, Object> fila : filas) {
                Producto p = new Producto();
                p.setCoProducto((String)fila.get("cod_producto"));
                p.setNombres((String)fila.get("nombres"));
                p.setEstado((String)fila.get("estado"));
                lista.add(p);
            }

        }catch (Exception ex){
            return lista;
        }
        return lista;
    }
    public List<Libro> obtenerLibros() {
        List<Libro> lista = null;
        try{
            List<Map<String, Object>> filas = this.jdbcTemplate.queryForList(
                    " select isbn, titulo, editorial,autor, to_char(fecha, 'YYYY-MM-DD HH12:MI:SS') fec " +
                            " from libro ");
            lista = new ArrayList<Libro>();
            for (Map<String, Object> fila : filas) {
                Libro p = new Libro();
                p.setIsbn((String)fila.get("isbn"));
                p.setAutor((String)fila.get("autor"));
                p.setEditorial((String)fila.get("editorial"));
                p.setFecha((String)fila.get("fec"));
                p.setTitulo((String)fila.get("titulo"));
                lista.add(p);
            }

        }catch (Exception ex){
            return lista;
        }
        return lista;
    }

    public Usuario loginUsuario(Usuario request) {
        Usuario usuario = null;
        try{
            usuario = this.jdbcTemplate.queryForObject(
                    " select cod_usuario, " +
                            "       credencial, " +
                            "       nombres, " +
                            "       apellidos, " +
                            "       estado from Usuario" +
                            " where cod_usuario = ? " +
                            " and credencial = ? ", new Object[]{request.getCoUsuario(),
                            request.getCredencial()}, new UsuarioMapper());
        }catch (Exception ex){
            ex.printStackTrace();
            return usuario;
        }
        return usuario;
    }
}
