drop table usuario;

create table Usuario(
    cod_usuario varchar(10),
    credencial varchar(10),
    nombres varchar(50),
    apellidos varchar(100),
    estado varchar(10)
);

select cod_usuario,
       credencial,
       nombres,
       apellidos,
       estado from Usuario
where cod_usuario = '123456789';
and credencial = '93852741';

select 'UNI'||trim(to_char(
          to_number(substr(max(cod_usuario),4,7),'9999999')+1
           ,'0000009')) cod_usuario
from Usuario;

insert into Usuario values
('123456789', '987654321', 'Juan', 'Perez', 'ACTIVE');

insert into Usuario values
('654321987', '93852741', 'Jose', 'Alvarez', 'ACTIVE');


create table producto(
                        cod_producto varchar(10),
                        nombres varchar(50),
                        estado varchar(1)
);

select cod_producto, nombres, estado,upper(nombres)
from producto
where estado = '1' and upper(nombres) like '%VINIFAN%';

insert into producto(cod_producto, nombres, estado) values('1','Marcador Vinifan A1','1');
insert into producto(cod_producto, nombres, estado) values('2','Marcador Vinifan A2','1');
insert into producto(cod_producto, nombres, estado) values('3','Marcador Vinifan A3','1');
insert into producto(cod_producto, nombres, estado) values('4','Marcador Vinifan A4','0');
insert into producto(cod_producto, nombres, estado) values('5','Marcador Vinifan A5','1');
insert into producto(cod_producto, nombres, estado) values('6','Lapiz Pilot','1');

create table estado(
    co_estado varchar(1),
    de_estado varchar(100)
);
insert into estado(co_estado, de_estado) values('0','No disponible');
insert into estado(co_estado, de_estado) values('1','Disponible');

select * from estado;

select p.cod_producto, p.nombres, p.estado, e.de_estado
from producto p
inner join estado e
on(p.estado=e.co_estado);


create table libro
(
    isbn varchar(9),
    titulo varchar(20),
    editorial varchar(20),
    autor varchar(20),
    fecha timestamp
);
insert INTO libro(isbn, titulo, editorial, autor, fecha) values('001','the reference','oracle press','Eduardo',CURRENT_TIMESTAMP);
insert INTO libro(isbn, titulo, editorial, autor, fecha) values('002','Alegria','oracle dwawda','Manuel villas',CURRENT_TIMESTAMP);
insert INTO libro(isbn, titulo, editorial, autor, fecha) values('003','como perder','Gucci producciones','Renzo',CURRENT_TIMESTAMP);
insert INTO libro(isbn, titulo, editorial, autor, fecha) values('004','Guia Dota 2','asdawwadwass','Smash',CURRENT_TIMESTAMP);
select isbn, titulo, editorial, autor, to_char(fecha, 'YYYY-MM-DD HH12:MI:SS') fec from libro;


select *--count(*)
    from libro;

select autor, count(*)
from libro
group by autor;


create table nota(
  curso varchar(20),
  tipo varchar(2),
  nota numeric(4,2)
);

insert into nota(curso, tipo, nota) values('OOP','EP',9.5);
insert into nota(curso, tipo, nota) values('OOP','EP',15.5);
insert into nota(curso, tipo, nota) values('OOP','EP',18.5);
insert into nota(curso, tipo, nota) values('OOP','EP',8.5);
insert into nota(curso, tipo, nota) values('OOP','EF',4.5);
insert into nota(curso, tipo, nota) values('OOP','EF',2.5);
insert into nota(curso, tipo, nota) values('OOP','EF',18.5);
insert into nota(curso, tipo, nota) values('OOP','PC',10.5);
insert into nota(curso, tipo, nota) values('OOP','PC',12.5);
insert into nota(curso, tipo, nota) values('FIS','EP',20);
insert into nota(curso, tipo, nota) values('FIS','EF',12);


SELECT curso, tipo,max(nota),min(nota)
FROM nota
group by curso,tipo
order by curso,tipo;


select curso, tipo,nota, substr(curso,1,2),substr(curso,2,2)
from nota

select 'UNI'||trim(to_char(           to_number(substr(max(cod_usuario),4,7),'9999999')+1           ,'0000009')) cod_usuario,   null credencial, null nombres, null apellidos, null estado from Usuario





