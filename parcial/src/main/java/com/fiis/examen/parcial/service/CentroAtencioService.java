package com.fiis.examen.parcial.service;

import com.fiis.examen.parcial.domain.Accidente;

public interface CentroAtencioService {
    void atender(Accidente accidente);
    default void terminarAtencion(){
        System.out.println("Se ha concluido la atención");
    }
}
