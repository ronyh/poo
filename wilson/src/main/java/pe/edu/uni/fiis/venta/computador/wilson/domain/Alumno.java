package pe.edu.uni.fiis.venta.computador.wilson.domain;

public class Alumno {

    /**
     * co_alumno varchar(9),
     *   nombres varchar(60),
     *   apellidos varchar(60),
     *   edad numeric(3,0)
     */
    private String coAlumno;
    private String nombres;
    private String apellidos;
    private Integer edad;

    public String getCoAlumno() {
        return coAlumno;
    }

    public void setCoAlumno(String coAlumno) {
        this.coAlumno = coAlumno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }
}
